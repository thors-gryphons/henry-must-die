using UnityEngine;

public class PlatformMovement : MonoBehaviour
{
    public float x = 1f;
    private Vector3 startPosition;
    [SerializeField] public float frequency = 5f;
    [SerializeField] public float magnitude = 5f;
    [SerializeField] public float offset = 0f;
    private void Start()
    {
        startPosition = transform.position;
    }
    private void Update()
    {
        transform.position = startPosition + x*transform.up* Mathf.Sin(Time.time * frequency + offset) * magnitude;
    }
}
