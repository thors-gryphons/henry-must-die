using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    
    Animator anim;
    public float fullHealth;
    public float currentHealth;
    public bool playerDied = false;
    public bool isInvulnerable = false;
    [SerializeField] HealthBar healthBar;
   
    public void Awake()
    {
        anim = GetComponent<Animator>();
        currentHealth = fullHealth;
       
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.tag == "Enemies")
        {
            AddDamage(10);
        }
        if (collision.gameObject.tag == "Trap")
        {
            AddDamage(5);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "PowerUP")
        {
            BonusHealth(20);
        }
    }

    public void AddDamage(float damage)
    {
        currentHealth -= damage;

         currentHealth = Mathf.Clamp(currentHealth, 0f, fullHealth);
        healthBar.SetHealth(100 * (currentHealth / fullHealth));
        anim.SetTrigger("Hurt");
        if (currentHealth <= 0)
        {
            anim.SetBool("Death", true);
            playerDied = true;
            this.enabled = false;
          GetComponent<HeroKnight>().enabled = false;
            Destroy(gameObject,1.5f);
        }
    }
    public void BonusHealth(float Bonus)
    {
        currentHealth += Bonus;
        currentHealth = Mathf.Clamp(currentHealth, 0f, fullHealth);
        healthBar.SetHealth(100 * (currentHealth / fullHealth));
    }
}

