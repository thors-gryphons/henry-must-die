using UnityEngine;

public class PatrolAI2 : MonoBehaviour
{
    #region Public Variables
    public float WalkSpeed;
    public float attackDistance;//Minimun distance for atks
    public float timer;//timer for cooldown between atks;
    public bool mustPatrol;
    public Rigidbody2D rb;
    public Transform groundCheck;
    public LayerMask groundLayer;
    public Transform Player;
    public Collider2D bodyCollider;
    #endregion
    #region Private Variables
    private bool mustFlip;
    private float distToPlayer;
    private bool Cooling;//check if enemy is cooling after atk
    private float intTimer;
    private float distance; //store the distance btwn enemy and player
    private bool attackMode;
    Animator anim;
    #endregion


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        mustPatrol = true;
        
    }
    void Update()
    {
        if (mustPatrol)
        {
            Patrol();
        }
        distToPlayer = Vector2.Distance(transform.position, Player.position);
        if (distToPlayer <= distance)
        {
            if (Player.position.x > transform.position.x && transform.localScale.x < 0 || Player.position.x < transform.position.x && transform.localScale.x > 0)
            {
                flip();
            }
            mustPatrol = false;
            rb.velocity = Vector2.zero;

            EnemyLogic();
        }
        else
        {
            mustPatrol = true;
            anim.SetBool("InRange", false);
        }
    }

    void Patrol()
    {
        if (mustFlip || bodyCollider.IsTouchingLayers(groundLayer))
        {
            flip();
        }
        rb.velocity = new Vector2(WalkSpeed * Time.fixedDeltaTime, rb.velocity.y);
    }
    void flip()
    {
        mustPatrol = false;
        transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
        WalkSpeed *= -1;
        mustPatrol = true;
    }
    void EnemyLogic()
    {

        distance = Vector2.Distance(transform.position, Player.transform.position);
        if (distance > attackDistance)
        {
            
            StopAttack();
        }
        if (attackDistance >= distance && Cooling == false)
        {
            attack();
        }
        if (Cooling)
        {
            cooldown();
            anim.SetBool("InRange", false);
        }
        void attack()
        {
            timer = intTimer;
            attackMode = true;
            anim.SetBool("InRange", true);
        }
        void StopAttack()
        {
            Cooling = false;
            attackMode = false;
            anim.SetBool("InRange", false);
        }
        void cooldown()
        {
            timer -= Time.deltaTime;
            if (timer <= 0 && Cooling && attackMode)
            {
                Cooling = false;
                timer = intTimer;
            }
        }
    }
}
