using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    Animator anim;
    public float fullHealth;
    public float currentHealth;
    public bool EnemyDied = false;
    
    public void Awake()
    {
        anim = GetComponent<Animator>();
        currentHealth = fullHealth;
    }
    
    public void TakeDamage(float Hit)
    {
        currentHealth -= Hit;
        currentHealth = Mathf.Clamp(currentHealth, 0f, fullHealth);
        anim.SetTrigger("IsHit");
        
       
        if (currentHealth <= 0)
        {
            anim.SetBool("isDead", true);
            EnemyDied = true;
            GetComponent<Collider2D>().enabled = false;
            this.enabled = false;
            GetComponent<PatrolAI>().enabled = false;
            Destroy(gameObject, 1f);
        }
    }
    
}
