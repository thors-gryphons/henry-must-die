using System.Collections;
using UnityEngine;

public class PatrolAI : MonoBehaviour
{

    public float WalkSpeed, range, timeBetweenShots, shootSpeed;
    public bool mustPatrol;
    public Rigidbody2D rb;
    public Transform groundCheck;
    private bool mustFlip, canShoot;
    public LayerMask groundLayer;
    public Transform Player,shootPos;
    private float distToPlayer;
    public GameObject bullet;
    Animator anim;
    public Collider2D bodyCollider;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        mustPatrol = true;
        canShoot = true;
    }
    void Update()
    {
        if (mustPatrol)
        {
            Patrol();
        }
        distToPlayer = Vector2.Distance(transform.position, Player.position);
        if (distToPlayer <= range)
        {
            if (Player.position.x > transform.position.x && transform.localScale.x < 0 || Player.position.x < transform.position.x && transform.localScale.x > 0)
            {
                flip();
            }
            mustPatrol = false;
            rb.velocity = Vector2.zero;
            if(canShoot)
            StartCoroutine(Attack());
            anim.SetBool("InRange", true);
        }
        else
        {
            mustPatrol = true;
            anim.SetBool("InRange", false);
        }
        }
        
        void Patrol()
        {
            if (mustFlip||bodyCollider.IsTouchingLayers(groundLayer))
            {
                flip();
            }
            rb.velocity = new Vector2(WalkSpeed * Time.fixedDeltaTime, rb.velocity.y);
        }
        void flip()
        {
            mustPatrol = false;
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            WalkSpeed *= -1;
            mustPatrol = true;
        }
         IEnumerator Attack()
         {
        canShoot = false;
        yield return new WaitForSeconds(timeBetweenShots);
        GameObject newbullet = Instantiate(bullet, shootPos.position, Quaternion.identity);
        newbullet.GetComponent<Rigidbody2D>().velocity = new Vector2(shootSpeed * WalkSpeed * Time.fixedDeltaTime,0);
        canShoot = true;
         }
        
    } 

