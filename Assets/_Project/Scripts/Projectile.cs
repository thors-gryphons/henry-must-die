using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
 
    public float DieTime;
    
    void Start()
    {
        StartCoroutine(CountDownTimer());
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            die();
        }     
        else if (collision.gameObject.tag == "Ground")
        {
            die();
        }
        
    }
    IEnumerator CountDownTimer()
    {
        yield return new WaitForSeconds(DieTime);
        die();
    }
    void die()
    {
        Destroy(gameObject);
    }
}
