using UnityEngine;

public class MeleeEnemy : MonoBehaviour
{
    #region Public Variables
    public Transform rayCast;
    public LayerMask rayCastMask;
    public float rayCastLength;
    public float attackDistance;//Minimun distance for atks
    public float timer;//timer for cooldown between atks;
    public float moveSpeed;
    public Transform LeftLimit;
    public Transform RightLimit;
    public Rigidbody2D rb2d;

    #endregion

    #region Private Variables
    private RaycastHit2D hit;
    private Transform target;
    private Animator anim;
    private float distance; //store the distance btwn enemy and player
    private bool attackMode;
    private bool inRange;//check if the player is in range
    private bool Cooling;//check if enemy is cooling after atk
    private float intTimer;
    #endregion

    private void Awake()
    {
        selectTarget();
        intTimer = timer;
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        if (!attackMode)
        {
            move();
            
        }
        if (!InsideTheLimits() && !inRange && !anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            
            selectTarget();
        }
        if (inRange)
        {
            hit = Physics2D.Raycast(rayCast.position, transform.right, rayCastLength, rayCastMask);
         
            RayCastDebugger();
        }
        //when player is detected 
        if(hit.collider != null)
        {
            EnemyLogic();
        }else if (hit.collider == null)
        {
            inRange = false;
        }
        if(inRange == false)
        {
            anim.SetBool("CanWalk", false); 
            StopAttack();
        }
    }
    void EnemyLogic()
    {
        distance = Vector2.Distance(transform.position, target.position);
        if (distance > attackDistance)
        {
           
            StopAttack();
        }
        else if (attackDistance >= distance && Cooling == false)
        {
            attack();
            anim.SetBool("CanWalk", false);
            anim.SetBool("InRange", true);
        }
        if (Cooling)
        {
            cooldown();
            anim.SetBool("InRange", false);
          
        }
    }
        void move()
        {
        anim.SetBool("CanWalk", true);
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            {
                Vector2 targetPosition = new Vector2(target.position.x, transform.position.y);
                transform.position = Vector2.MoveTowards(transform.position, targetPosition, moveSpeed * Time.deltaTime);
           
            }
        }
        void attack()
        {
            timer = intTimer;
            attackMode = true;
           
            anim.SetBool("InRange", true);
        }
        void StopAttack()
        {
            Cooling = false;
            attackMode = false;
            anim.SetBool("InRange", false);
            anim.SetBool("CanWalk", true);
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            target = collision.transform;
            inRange = true;
            Flip(); 
        }
    }
    public void triggerCooling()
    {
        Cooling = true;
    }
    void cooldown()
    {
        timer -= Time.deltaTime;
        if (timer <= 0 && Cooling && attackMode)
        {
            Cooling = false;
            timer = intTimer;
        }
    }
    void RayCastDebugger()
    {
        if (distance > attackDistance)
        {
            Debug.DrawRay(rayCast.position,transform.right*rayCastLength,Color.red);
        }
        else if (attackDistance > distance)
        {
            Debug.DrawRay(rayCast.position, transform.right * rayCastLength, Color.green);

        }
    }
    private bool InsideTheLimits()
    {
        return transform.position.x > LeftLimit.position.x && transform.position.x < RightLimit.position.x;
    }
    void selectTarget()
    {
        float distanceToLeft = Vector2.Distance(transform.position, LeftLimit.position);
        float distanceToRight = Vector2.Distance(transform.position, RightLimit.position);
        if (distanceToLeft > distanceToRight)
        {
            target = LeftLimit;
        }
        else
        {
            target = RightLimit;
        }
        Flip();
    }
    private void Flip() {
        Vector3 rotation = transform.eulerAngles;
        if (transform.position.x > target.position.x)
        {
            rotation.y = 180f;
        }
        else
        {
            rotation.y = 0f;
        }
        transform.eulerAngles = rotation;
    }

}
